# CHANGELOG

## Version 3.3.0.1 (18/07/2024)

### Updated

* Updated for Monetizr v1.0.8

## Version 3.3.0.0 (01/01/2024)

### Updated

* Update architecture for 3.3
