# Monetizr

Monetizr is an in-game rewards platform that provide "playable" sponsored campaigns, used to reward the users with in-game items, currency, or other rewards.

For more information, please refer to the [Monetizr official website](https://monetizr.com/brands).

## Integration steps

To easily integrate it in FunGames environment, please follow the steps below:

1) **Install** or **Upload** FGMonetizr plugin from the FunGames Integration Manager, or download it from here
2) **Import** the latest Monetizr Unity SDK through the Package Manager, using the following git URL: https://github.com/themonetizr/The-Monetizr-Campaigns-Unity-SDK.git?path=/Assets
3) **Click on the "Prefab" button** to add the FGMonetizr prefab into your main scene, this will also create the
   FGMonetizrSettings in _Assets/Resources/Fungames_ folder.
4) Ask your Publisher or LiveOps manager to provide you the **API Key** corresponding to your app and add it to the **FGMonetizrSettings**

## Scripting API

To properly setup Monetizr with Reward Assets and Mission Descriptions, you'll only need to call ```FGMonetizr.AddGameCoinAsset``` and  ```FGMonetizr.AddMissionDescription``` (optional) on Awake().

As an example on how to add rewards to Monetizr, you can check the FGMonetizrExample.cs script (in *Assets/FunGames/Monetization/SponsoredCampaigns/Monetizr/_Debug*).